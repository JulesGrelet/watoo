public interface Vendable {

    public String getType();
    public String getNom();
    public double getEtat();
    public String getStrEtat();
    public String getInfos(String fonctionOuClasse);
    public void reparer();

}