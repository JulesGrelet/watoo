public class Droide extends Article {

    private String fonction;

    public Droide(String leNom, double lEtat, String laFonction, String lImage) {
        super(leNom, lEtat, lImage);
        this.fonction = laFonction;
    }

    public String getFonction () {
        return this.fonction;
    }

}