public class Vaisseau extends Article {

    private String classe;

    public Vaisseau(String leNom, double lEtat, String laClasse, String lImage) {
        super(leNom, lEtat, lImage);
        this.classe = laClasse;
    }

    public String getClasse () {
        return this.classe;
    }

}