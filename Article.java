public class Article implements Vendable {

    protected String nom;
    protected double etat;
    protected String image;

    public Article (String leNom, double lEtat, String lImage) {
        this.nom = leNom;
        this.etat = lEtat;
        this.image = lImage;
    }

    @Override
    public String getNom () {
        return this.nom;
    }

    @Override
    public double getEtat () {
        return this.etat;
    }

    @Override
    public String getStrEtat () {
        String strEtat="";
        if (this.getEtat()<10)
            strEtat = "poubelle";
        else if (this.getEtat()>=10 && this.getEtat()<=30)
            strEtat = "mauvais";
        else if (this.getEtat()>=40 && this.getEtat()<=60)
            strEtat = "bon";
        else if (this.getEtat()>=70 && this.getEtat()<=99)
            strEtat = "tres bon";
        else if (this.getEtat()==100)
            strEtat = "comme neuf";
        return strEtat;
    }

    @Override
    public String getType() {
        if (this instanceof Droide)
            return "Droide";
        else if (this instanceof Vaisseau)
            return "Vaisseau";
        else
            return "";
        
    }

    @Override
    public String getInfos(String fonctionOuClasse) {
        String str="";
        if (this.getType().equals("Droide"))
            str = "Fonction";
        else if (this.getType().equals("Vaisseau"))
            str = "Classe";
        return "- Type : "+this.getType()+"\n"+
               "- Nom : "+this.getNom()+"\n"+
               /*"- "+str+" : "+fonctionOuClasse+"\n"+*/
               "- Etat : "+this.getStrEtat()+" ("+this.getEtat()+"%)";
    }

    @Override
    public void reparer () {
        if (this.etat!=100)
            this.etat+=10;
    }

    public String getImage () {
        return this.image;
    }



}