public class Executable {
    public static void main(String [] args){
        
        Stock lestock = new Stock ("jules");
        Droide dum = new Droide ("dum", 50, "mecanicien", "img");
        Droide gonk = new Droide ("gonk", 80, "energetique", "img");
        Vaisseau vaiss1 = new Vaisseau("patrouille mante", 40, "chasseur", "img");
        Vaisseau vaiss2 = new Vaisseau("attaque rapide", 60, "combat leger", "img");
    
        lestock.ajouter(dum);
        lestock.ajouter(vaiss1);
        lestock.ajouter(vaiss2);
        lestock.ajouter(gonk);
        for (Vendable artcl : lestock.getLeStock() ) {
            System.out.println(artcl.getNom()+" : "+artcl.getType());
        }
        System.out.println("-------------------");

        lestock.vendre(gonk);
        for (Vendable artcl : lestock.getLeStock() ) {
            System.out.println(artcl.getNom()+" : "+artcl.getType());
        }
        System.out.println("-------------------");

        System.out.println(lestock.getArticleEnPresentation().getNom());
        lestock.suivant();
        System.out.println(lestock.getArticleEnPresentation().getNom());
        lestock.precedent();
        System.out.println(lestock.getArticleEnPresentation().getNom());
        lestock.precedent();
        System.out.println(lestock.getArticleEnPresentation().getNom());
        System.out.println("-------------------");
        
        Vendable current = lestock.getArticleEnPresentation();
        System.out.println(current.getNom()+", etat : "+current.getEtat()+"%");
        current.reparer();
        System.out.println(current.getNom()+", etat : "+current.getEtat()+"%");
        current.reparer();
        current.reparer();
        System.out.println(current.getNom()+", etat : "+current.getEtat()+"%");
        current.reparer();
        current.reparer();
        System.out.println(current.getNom()+", etat : "+current.getEtat()+"%");
        System.out.println("-------------------");

        String data = "";/*
        if (current.getType().equals("Droide"))
            data = current.getFonction();
        else if (current.getType().equals("Vaisseau"))
            data = current.getClasse();*/

        System.out.println(current.getInfos(data));
        System.out.println("\n");
        lestock.suivant();
        System.out.println(lestock.getArticleEnPresentation().getInfos(data));
    }
}
