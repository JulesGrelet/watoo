import java.util.List;
import java.util.ArrayList;

public class Stock {

    private String proprietaire;
    private List<Vendable> leStock;
    private Vendable articleEnPresentation;

    public Stock(String proprio) {
        this.proprietaire = proprio;
        this.leStock = new ArrayList<Vendable>();
    }

    public List<Vendable> getLeStock () {
        return this.leStock;
    }

    public Vendable getArticleEnPresentation () {
        return this.articleEnPresentation;
    }

    public void vendre (Vendable lArticle) {
        if (this.leStock.contains(lArticle))
            this.leStock.remove(lArticle);
    }

    public void ajouter (Vendable lArticle) {
        if (!this.leStock.contains(lArticle))
            this.leStock.add(lArticle);
        if (this.articleEnPresentation==null)
            this.articleEnPresentation=lArticle;
    }

    public void suivant() {
        int i = this.leStock.indexOf(this.articleEnPresentation);
        if (i+1==this.leStock.size())
            i=0;
        else
            i++;
        this.articleEnPresentation=this.leStock.get(i);
    }

    public void precedent() {
        int i = this.leStock.indexOf(this.articleEnPresentation);
        if (i==0)
            i=this.leStock.size()-1;
        else
            i--;
        this.articleEnPresentation=this.leStock.get(i);
    }

}